import os

from flask import Flask, request, jsonify
from werkzeug.contrib.fixers import ProxyFix
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from construct import Construct, ServiceAuthPipe

app = Flask(__name__)

app.config.from_json('config.json')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

construct = Construct(app, db)
service_pipe = ServiceAuthPipe(construct)

@app.route('/')
def index():
    return 'It works!'

app.wsgi_app = ProxyFix(app.wsgi_app)
if __name__ == '__main__':
    app.run(host=app.config['RUN_HOST'], port=app.config['RUN_PORT'], debug=app.config['DEBUG_MODE'])
    
#!/bin/sh

REPO="https://gitlab.com/gcomapps-public/mirrar/base/raw/master"

git submodule add https://agratoth:2xmcAsJTBBhfgoZc_cDb@gitlab.com/gcomapps/mirrar/construct.git construct

wget -q "$REPO/config.debug.json" -O "config.debug.json"
wget -q "$REPO/.gitignore" -O ".gitignore"
wget -q "$REPO/bootstrap.py" -O "bootstrap.py"
wget -q "$REPO/build.json" -O "build.json"
wget -q "$REPO/Dockerfile" -O "Dockerfile"
wget -q "$REPO/pip.txt" -O "pip.txt"
wget -q "$REPO/service.py" -O "service.py"
FROM python:3.6-alpine

RUN adduser -D service

WORKDIR /home/service

RUN apk add --no-cache postgresql-libs
RUN apk add --no-cache --virtual .build-deps git gcc musl-dev postgresql-dev freetype-dev jpeg-dev zlib-dev

COPY pip.txt pip.txt
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r pip.txt --no-cache-dir
RUN venv/bin/pip install gunicorn

COPY construct construct
COPY bootstrap.py service.py config.debug.json ./
RUN venv/bin/python bootstrap.py

ENV FLASK_APP service.py
ENV LC_ALL en_US.utf-8
ENV LANG en_US.utf-8

RUN chown -R service:service ./

USER service

EXPOSE <port>
ENTRYPOINT ["./boot.sh"]

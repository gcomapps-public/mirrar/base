#!../venv/bin/python

import os, json, subprocess
from requests import post

def set_value(key, dictio, default):
    return dictio[key] if key in dictio else default

def is_debug():
    return not os.environ.get('PRODUCTION', False)

def get_token(url, service, key):
    try:
        response = post(url, json={'service':service, 'key':key})
        resp_data = response.json()
    except:
        resp_data = {}
    return set_value('token', resp_data, None)

def get_key(url):
    try:
        response = post(url)
        resp_data = response.json()
    except:
        resp_data = {}
    return set_value('key', resp_data, None)

config_file = 'config.debug.json' if is_debug() else 'config.production.json'
config = json.loads(open(config_file).read())

work_config = {}
work_config['RUN_HOST'] = set_value('RUN_HOST', config, '0.0.0.0')
work_config['RUN_PORT'] = set_value('RUN_PORT', config, '5000')
work_config['DEBUG_MODE'] = is_debug()
work_config['GI_WORKERS'] = set_value('GI_WORKERS', config, 4)
work_config['GI_TIMEOUT'] = set_value('GI_TIMEOUT', config, 500)
work_config['SQLALCHEMY_DATABASE_URI'] = set_value('SQLALCHEMY_DATABASE_URI', config, '')
work_config['SQLALCHEMY_TRACK_MODIFICATIONS'] = set_value('SQLALCHEMY_TRACK_MODIFICATIONS', config, False)
work_config['SERVICE_AUTH_HOST'] = set_value('SERVICE_AUTH_HOST', config, '')
work_config['SERVICE_ID'] = set_value('SERVICE_ID', config, '')
work_config['SERVICE_KEY'] = set_value('SERVICE_KEY', config, '')
work_config['SERVICE_TOKEN'] = get_token('{}/auth'.format(work_config['SERVICE_AUTH_HOST']), work_config['SERVICE_ID'], work_config['SERVICE_KEY'])
work_config['SERVICE_PUBLIC'] = get_key('{}/key'.format(work_config['SERVICE_AUTH_HOST']))
open('config.json', 'w').write(json.dumps(work_config))

boot_script = '#!/bin/sh\n'
boot_script += 'source venv/bin/activate\n'
boot_script += 'flask db migrate\n'
boot_script += 'flask db upgrade\n'
boot_script += 'exec gunicorn -b :{} --access-logfile - --error-logfile - service:app --workers {} --timeout {}'.format(work_config['RUN_PORT'], work_config['GI_WORKERS'], work_config['GI_TIMEOUT'])
open('boot.sh', 'w').write(boot_script)

run_script = '#!/bin/sh\n'
run_script += 'source ../venv/bin/activate\n'
run_script += 'gunicorn -b :{} --access-logfile - --error-logfile - service:app --workers {} --timeout {}'.format(work_config['RUN_PORT'], work_config['GI_WORKERS'], work_config['GI_TIMEOUT'])
open('run.sh', 'w').write(run_script)

subprocess.run(['chmod', '+x', 'boot.sh'])
subprocess.run(['chmod', '+x', 'run.sh'])